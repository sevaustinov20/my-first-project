﻿// See https://aka.ms/new-console-template for more information

namespace HomeWork
{
    static class Program
    {
        public static void Main(string[] args)
        {
            Player player = new Player("Seva", "Ustinov", "seva1337");
            player.NickName = "SEVA";
            String firstName = player.FirstName;
            Console.WriteLine(firstName);
            Score score = new Score(0, player, DateTime.Now);
            score.Value = 1;
            Console.WriteLine(player);
            Console.WriteLine(score);
            
        }
    }
}