﻿namespace HomeWork;

public class Score
{
    private int value;
    private Player player;
    private DateTime date;

    public Player Player
    {
        get
        {
            return player;
        }
    }

    public int Value
    {
        get
        {
            return value;
        }
        set
        {
            this.value = value;
        }
    }

    public DateTime Date
    {
        get
        {
            return date;
        }
        set
        {
            this.date = date;
        }
    }
    

    public Score(int value, Player player, DateTime date)
    {
        this.value = value;
        this.player = player;
        this.date = date;
    }
    

    public override string ToString()
    {
        return player.NickName + " : " + value + "\t" + date;
    }
}