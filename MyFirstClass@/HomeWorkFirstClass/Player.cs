﻿namespace HomeWork;

public class Player
{
    private string firstName;
    private string lastName;
    private string nickname;

    public string FirstName
    {
        get
        {
            return firstName;
        }
        set
        {
            this.firstName = firstName;
        }
    }

    public string NickName
    {
        get
        {
            return nickname;
        }
        set
        {
            nickname = value;
        }
    }

    public string LastName
    {
        get
        {
            return lastName;
        }
        set
        {
            lastName = nickname;
        }
        
    }

    public Player(string firstName, string lastName, string nickname)
    {
        this.firstName = firstName;
        this.lastName = lastName;
        this.nickname = nickname;
    }

    public override string ToString()
    {
        return nickname;
        
    }
}