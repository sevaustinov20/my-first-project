﻿// See https://aka.ms/new-console-template for more information

namespace ClassWork
{
    class Lesson
    {
        public int duration;
        public string student;

        public Lesson()
        {
            
        }

        public Lesson(int duration, string student)
        {
            this.student = student;
            this.duration = duration;
        }

        public override string ToString()
        {
            return duration.ToString();
        }
    }

    class Example
    {
        public string name;

        public Example()
        {
            Console.WriteLine("Hello");
        }

        public Example(string name)
        {
            this.name = name;
        }

        public override bool Equals(object obj)
        {
            Example ex = (Example)obj;
            return name == ex.name;
        }

        public static bool operator ==(Example example, Example example1)
        {
            return example.name == example1.name;
        }   
        
        public static bool operator !=(Example example, Example example1)
        {
            return example.name != example1.name;
        }
    }

    static class Program
    {
        public static void Main(string[] args)
        {
            // Lesson lesson = new Lesson();
            // lesson.duration = 15;
            // lesson.student = "Seva";
            // Lesson lessonTwo = new Lesson(45, "Dmitry");
            // Console.WriteLine(lesson.ToString());
            // Console.WriteLine(lessonTwo.ToString());

            Example ex = new Example();
            Console.WriteLine("Seva Hello");
            ex.name = "Seva";
            Example ex1 = new Example("Seva");
        }
    }


}








