﻿using System;

namespace ConsoleApp7
{
    class Program
    {
        static void Main(string[] args)
        {
            // string a = "15";
            // int b = int.Parse(a);
            char c;
            bool canConvert = char.TryParse("c", out c);
            if (canConvert)
            {
                Console.WriteLine(char.ToUpper(c));
            }
            bool e = Convert.ToBoolean(0);
            if (e)
                Console.WriteLine(e);
        }
    }
}