﻿using System;

namespace ConsoleApp1
{
    class Program
    {
        static void Main(string[] args)
        {
            LogIn("Seva", "12345");
        }


        private static void LogIn(string name, string pass)
        {
            bool isSuccess = false;

            for (int attempts = 0; attempts < 3; attempts++)
            {
                // Console.WriteLine("Введите имя пользователя и пароль");
                Console.WriteLine("Введите имя пользователя: ");
                string inputUsername = Console.ReadLine();
                Console.WriteLine("Введите пароль: ");
                string inputPassword = Console.ReadLine();


                if (name == inputUsername && pass == inputPassword)
                {
                    isSuccess = true;
                    break;
                }
            }

            if (isSuccess)
                Console.WriteLine("Добро пожаловать {0}!", name);
            else
                Console.WriteLine("Неверный логин или пароль.");
        }
    }
}